module Translation(translate, translate2) where

import Terms


abstract :: VarT -> SK -> SK
abstract x (VarCL y) | x == y = AppCL (AppCL S K) K
abstract x m | x `notElem` freeVars m = AppCL K m
abstract x (AppCL m n) = AppCL (AppCL S (abstract x m)) (abstract x n)


abstract2 :: SK -> Direction -> SK
abstract2 t None = AppCL K t
abstract2 (VarCL x) End = AppCL (AppCL S K) K
abstract2 (AppCL m n) (Dir left right) = AppCL (AppCL S (abstract2 m left)) (abstract2 n right)


abstract3 :: VarT -> SKe -> SKe
abstract3 x t = abstract' t $ freeVars3 x t


abstract' :: SKe -> Direction -> SKe
abstract' t None = AppCLe (Trash $ TrashT Kc 0) t (hasVars t)
abstract' (VarCLe _) End = AppCLe (AppCLe (Cons Sc) (Cons Kc) False) (Cons Kc) False
abstract' (AppCLe m n _) (Dir left right) =
    let leftVal = abstract' m left
        rightVal = abstract' n right
        leftHas = hasVars leftVal
        hasVariables = leftHas || hasVars rightVal
    in
    AppCLe (AppCLe (Trash (TrashT Sc 0)) leftVal leftHas) rightVal hasVariables


translate :: Lambda -> SK
translate (VarL x) = VarCL x
translate (AppL m n) = AppCL (translate m) (translate n)
-- translate (Abs x m) = let translated = translate m; freeVars = freeVars2 x translated in
--   abstract2 translated freeVars
translate (Abs x m) = abstract x (translate m)


translate2 :: Lambda -> SKe
translate2 (VarL x) = VarCLe x
translate2 (AppL m n) = let left = translate2 m; right = translate2 n in
  AppCLe left right (hasVars left || hasVars right)
translate2 (Abs x m) = abstract3 x $ translate2 m


freeVars :: SK -> [VarT]
freeVars S = []
freeVars K = []
freeVars (VarCL x) = [x]
freeVars (AppCL t1 t2) = freeVars t1 ++ freeVars t2


freeVars2 :: VarT -> SK -> Direction
freeVars2 _ S = None
freeVars2 _ K = None
freeVars2 x (VarCL y) | x == y = End
                      | otherwise = None
freeVars2 x (AppCL t1 t2) = let left = freeVars2 x t1; right = freeVars2 x t2 in
  case (left, right) of
    (None, None) -> None
    _ -> Dir left right


freeVars3 :: VarT -> SKe -> Direction
freeVars3 x (VarCLe y) | x == y = End
                      | otherwise = None
freeVars3 _ (AppCLe _ _ False) = None
freeVars3 x (AppCLe t1 t2 True) = let left = freeVars3 x t1; right = freeVars3 x t2 in
  case (left, right) of
    (None, None) -> None
    _ -> Dir left right
freeVars3 _ _ = None


hasVars :: SKe -> Bool
hasVars (AppCLe _ _ val) = val
hasVars (VarCLe _) = True
hasVars _ = False


nextTrash :: TrashTerm -> TrashTerm
nextTrash (TrashT v val) = TrashT v $! val+1

worst :: Int -> Lambda
worst n = foldl AppL (VarL 1) $ VarL <$> [2..n]


worstCase :: Int -> Lambda
worstCase n = foldr Abs (worst n) $ reverse [1..n]


worstCase2 :: Int -> Lambda
worstCase2 n = foldr Abs (worst n) [1..n]


worstCase3 :: Int -> Lambda
worstCase3 n = foldr Abs (AppL (worst n) (worst n)) [1..n]


worstCase4 :: Int -> Lambda
worstCase4 1 = Abs 1 $ worst 1
worstCase4 n = foldr Abs (AppL (worst n) (worst (n-1))) [1..n]


worstCase3k :: Int -> Lambda
worstCase3k n = foldr Abs (worst n) [1..n+1]


worstCase3k1 :: Int -> Lambda
worstCase3k1 n = foldr Abs (AppL (worst n) (VarL 1)) [1..n]


instance Show SK where
    showsPrec _ (VarCL var) = shows var
    showsPrec p (AppCL m n) =
      let arg = if size n > 1 then showChar '(' . showsPrec p n . showChar ')'
                              else showsPrec p n
      in
      showsPrec p m . showChar ' ' . arg
    showsPrec _ S = showChar 'S'
    showsPrec _ K = showChar 'K'


instance Show Lambda where
    showsPrec _ (VarL var) = shows var
    showsPrec p (AppL m n) =
      let arg = if sizeL n > 1 then showChar '(' . showsPrec p n . showChar ')'
                               else showsPrec p n
      in
      showChar '(' . showsPrec p m . showChar ' ' . arg . showChar ')'
    showsPrec p (Abs var m) = showChar 'l' . showsPrec p (VarL var) . showChar '.' . showsPrec p m


instance Show SKe where
    showsPrec _ (VarCLe var) = shows var
    showsPrec p (AppCLe m n _) =
      showChar '(' . showsPrec p m . showChar ' ' . showsPrec p n . showChar ')'
    showsPrec p (Cons cons) = showsPrec p cons
    showsPrec p (Trash t) = showsPrec p t


instance Show TrashTerm where
    showsPrec p (TrashT t val) = showsPrec p (trashTermsWith t !! val)


instance Show Constant where
    showsPrec _ Sc = showChar 'S'
    showsPrec _ Kc = showChar 'K'
    showsPrec _ Bc = showChar 'B'
    showsPrec _ Cc = showChar 'C'


fromType :: Constant -> SKe
fromType = Cons


trashTermsWith :: Constant -> [SKe]
trashTermsWith tType = let res = fromType tType : map newTerm res in res
  where newTerm t = AppCLe (Cons Sc) (AppCLe (Cons Kc) t False) False


test :: IO ()
test = do
    let term = Abs 2 (Abs 1 (AppL (VarL 2) (VarL 1)))
        sizeT = sizeL term
        translated = translate term
        sizeAfter = size translated
    putStrLn $ "Term: " ++ show term
    putStrLn $ "Initial size: " ++ show sizeT
    putStrLn $ "Translated size: " ++ show sizeAfter
    putStrLn $ "Translated term: " ++ show translated
