module GenerateTerms(worstCaseSize, listOfWorstCases, generateTerms, fromDeBruijn) where

import Data.List

import Terms
import Translation


vars :: [DeBruijn]
vars = VarD <$> [1..]


generateTerms :: Int -> [DeBruijn]
generateTerms size = terms size 1


generateTerms2 :: Int -> [Lambda]
generateTerms2 size = terms2 [1..size] !! (size-1)


terms :: Int -> Int -> [DeBruijn]
terms size varLimit | size == 1 = take varLimit vars
                    | otherwise = (LambdaD <$> terms (size-1) (varLimit+1)) ++ appTerms
  where appTerms = concat $ (\n -> AppD <$> terms n varLimit <*> terms (size-1-n) varLimit) <$> [1..(size-2)]


fromDeBruijn :: DeBruijn -> Lambda
fromDeBruijn m = fromDeBruijn' m 1


fromDeBruijn' :: DeBruijn -> VarT -> Lambda
fromDeBruijn' (VarD var) lambda = VarL $ lambda - var + 1
fromDeBruijn' (AppD m n) lambda = AppL (fromDeBruijn' m lambda) (fromDeBruijn' n lambda)
fromDeBruijn' (LambdaD m) lambda = let nextVar = lambda+1 in Abs (nextVar) $ fromDeBruijn' m nextVar


worstCaseSize :: Int -> (Int, Lambda, SKe)
worstCaseSize termSize =
    let terms = (\(l, c) -> (sizeSKe c, l, c)) <$> (\term -> (term, translate2 term)) <$> fromDeBruijn <$> generateTerms termSize
        max = maximumBy (\(s1, _, _) (s2, _, _) -> compare s1 s2) terms
    in max


listOfWorstCases :: [(Int, Lambda, SKe)]
listOfWorstCases = worstCaseSize <$> [1..]


terms2 :: [VarT] -> [[Lambda]]
terms2 vars = terms
  where terms = (VarL <$> vars) : (Abs <$> vars <*> ofSize 1) : rest
        rest = (\size -> ((Abs <$> vars <*> ofSize (size-1)) ++ appTerms size)) <$> [3..]
        ofSize n = terms !! (n - 1)
        appTerms size = concat $ (\n -> AppL <$> ofSize n <*> ofSize (size-1-n)) <$> [1..(size-2)]
