module Main where

import System.IO

import GenerateTerms


main :: IO ()
main = do
  putStr "Input the size of a lambda term: "
  hFlush stdout
  termSize <- read <$> getLine
  let (size, term, translation) = worstCaseSize termSize
  putStrLn $ "Size: " ++ show size
  putStrLn $ "Term: " ++ show term
  putStrLn $ "Translation: " ++ show translation
  print $ length $ fromDeBruijn <$> generateTerms termSize
