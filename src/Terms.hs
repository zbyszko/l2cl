module Terms where


type VarT = Int

data SK = VarCL VarT | S | K | AppCL SK SK deriving Eq

data Lambda = VarL VarT | AppL Lambda Lambda | Abs VarT Lambda deriving Eq

data DeBruijn = VarD VarT | AppD DeBruijn DeBruijn | LambdaD DeBruijn deriving Show

data Direction = Dir Direction Direction | None | End

data Constant = Sc | Kc | Bc | Cc deriving Eq

data TrashTerm = TrashT Constant Int deriving Eq

data SKe = VarCLe VarT | Cons Constant | AppCLe SKe SKe Bool | Trash TrashTerm deriving Eq


size :: SK -> Int
size (VarCL _) = 1
size (S) = 1
size (K) = 1
size (AppCL m n) = 1 + size m + size n


sizeL :: Lambda -> Int
sizeL (VarL _) = 1
sizeL (AppL m n) = 1 + sizeL m + sizeL n
sizeL (Abs _ m) = 1 + sizeL m


sizeSKe :: SKe -> Int
sizeSKe (VarCLe _) = 1
sizeSKe (Cons _) = 1
sizeSKe (AppCLe m n _) = 1 + sizeSKe m + sizeSKe n
sizeSKe (Trash t) = sizeT t


sizeT :: TrashTerm -> Int
sizeT (TrashT _ val) = 4 * val + 1

